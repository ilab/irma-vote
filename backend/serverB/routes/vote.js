const express = require('express')
const conf = require('./../config/conf.json')
const IrmaBackend = require('@privacybydesign/irma-backend')

const irmaBackend = new IrmaBackend(conf.irma.url, {
  serverToken: conf.irma.auth_token,
})

var router = express.Router()

router.post('/:id/start', (req, res) => {
  // TODO: check if the election is still ongoing
  // We need the election data for this.

  // TODO: Check if an election with id exists in administration table

  return irmaBackend
    .startSession({
      '@context': 'https://irma.app/ld/request/signature/v2',
      message: req.body.text,
      disclose: [
        [
          [
            { type: conf.numberId, value: null },
            {
              type: conf.nameId,
              value: req.params.id,
            },
          ],
        ],
      ],
    })
    .then(({ sessionPtr, token }) => {
      req.session.token = token
      req.session.voted = false
      return res.status(200).json(sessionPtr)
    })
    .catch((err) => {
      console.log(err)
      return res.status(405).json({ err: err.message })
    })
})

router.get('/:id/finish', (req, res) => {
  if (req.session.voted) return res.status(403).json({ err: 'ALREADY_VOTED' })

  return irmaBackend.getSessionResult(req.session.token).then((result) => {
    if (result.status !== 'DONE' || result.proofStatus !== 'VALID')
      return res.status(403).json({ err: 'NOT_VALID' })

    let id = result.disclosed[0].find((attr) => attr.id == conf.nameId).rawvalue
    if (req.params.id !== id)
      return res.status(403).json({
        err: `election id within card does not match url, card: ${id}, url: ${req.params.id}`,
      })

    // TODO: Throw out whatever is not neccesary in the signature
    let sig = JSON.stringify(result.signature)
    let votingnumber = result.disclosed[0].find(
      (attr) => attr.id == conf.numberId
    ).rawvalue

    let message = result.signature.message
    console.log(id, votingnumber, message)

    try {
      req.db.transaction(() => {
        // Store signature under the unique combination of election name and voting number
        // This means the same votingnumber cannot have multiple rows for that election in the result table.
        req.db
          .prepare(
            `INSERT INTO results (id, votingnumber, signature) VALUES (?, ?, ?);`
          )
          .run([id, votingnumber, sig])

        req.db
          .prepare(
            `UPDATE elections SET participants = participants + 1 WHERE id = ?`
          )
          .run(req.params.id)
      })()
    } catch (err) {
      if (err) {
        // Most likely: the uniqueness constraint is violated, could check for this.
        console.log('error during storing of result: ', err)
        return res.status(403).json({ err: err.message }).end()
      }
    }

    // Mark the voting session completed. This is only to cancel the session in earlier stages.
    req.session.voted = true
    return res.status(200).end()
  })
})

module.exports = router
