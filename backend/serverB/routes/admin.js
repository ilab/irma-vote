var express = require('express')
var router = express.Router()
const IrmaBackend = require('@privacybydesign/irma-backend')

const conf = require('../config/conf.json')

const irmaBackend = new IrmaBackend(conf.irma.url, {
  serverToken: conf.irma.auth_token,
})

// middleware that is specific to this router
// maybe: move to PROJECT_ROOT/middleware?
router.use((req, res, next) => {
  if (
    req.session.admin_auth ||
    req.url === '/login/start' ||
    req.url === '/login/finish'
  )
    return next()
  res.status(403).json({ err: 'no cookie' })
})

// only admin route that does not require authentication
router.get('/login/start', (req, res) => {
  irmaBackend
    .startSession({
      '@context': 'https://irma.app/ld/request/disclosure/v2',
      disclose: [[['pbdf.sidn-pbdf.email.email']]],
    })
    .then(({ sessionPtr, token }) => {
      req.session.admin_auth = false
      req.session.admin_token = token
      return res.status(200).json(sessionPtr)
    })
    .catch((err) => res.status(403).json({ err: err.message }))
})

router.get('/login/finish', (req, res) => {
  irmaBackend
    .getSessionResult(req.session.admin_token)
    .then((result) => {
      if (!(result.proofStatus === 'VALID' && result.status === 'DONE'))
        throw new Error('NOT_DONE_VALID')

      let mail = result.disclosed[0][0].rawvalue
      if (!conf.admins.includes(mail)) throw new Error('NOT_AN_ADMIN')

      req.session.admin_auth = true
      res.status(200).json({ msg: 'success' })
    })
    .catch((err) => res.status(200).json({ msg: 'fail', err: err.message }))
})

router.get('/logout', (req, res) => res.clearCookie('session'))

router.get('/elections', (req, res) => {
  try {
    let rows = req.db.prepare('SELECT * FROM elections').all()
    res.status(200).json(rows)
  } catch (err) {
    res.status(500).json({ err: err.message })
  }
})

// new: create a new election
router.post('/new', (req, res) => {
  let db = req.db
  let stmt = db.prepare(
    `INSERT INTO elections (id, title, description, question, options, end, participants) VALUES (?, ?, ?, ?, ?, ?, ?);`
  )
  let data = req.body

  // dd-mm-yyyy -> yyyy-mm-dd
  let convert_date = (dateStr) => dateStr.split('-').reverse().join('-')

  // May only contain alphanumerical + underscores
  if (!data['election-id'].match(/^[a-z0-9_]+$/i))
    return res.status(403).json({ err: 'BAD_ELECTION_ID' })

  // TODO: more validation?

  let params = [
    data['election-id'],
    data['election-title'],
    data['election-description'],
    data['election-question'],
    data['election-options'],
    convert_date(data['election-end']),
    0,
  ]
  console.log(params)
  try {
    stmt.run(params)
  } catch (err) {
    if (err) {
      return res.status(403).json({ err: err.message })
    }
  }
  return res.status(200).json({ msg: 'success' })
})

// update: update an election
router.post('/:id/update', (req, res) => {
  // These columns are allowed to be changed
  const allowed = [
    'election-title',
    'election-question',
    'election-description',
    'election-options',
    'election-end',
  ]

  const filtered = Object.keys(req.body)
    .filter((key) => allowed.includes(key))
    .reduce((obj, key) => {
      return {
        ...obj,
        [key]: req.body[key],
      }
    }, {})

  let str = Object.keys(filtered)
    .map((k) => `${k.split('-')[1]} = \'${filtered[k]}\'`)
    .join(', ')

  // No on-going elections are allowed to be changed..
  let stmt = `UPDATE elections SET ${str} WHERE id = ? AND (DATETIME('now')) < start`
  console.log(stmt)
  try {
    req.db.prepare(stmt).run(req.params.id)
    res.status(204).end()
  } catch (err) {
    console.log(err)
    res.status(500).json({ err: err.message })
  }
})

router.delete('/:id/delete', (req, res) => {
  try {
    req.db.transaction(() => {
      req.db.prepare('DELETE FROM results WHERE id = ?').run(req.params.id)
      req.db.prepare('DELETE FROM elections WHERE id = ?').run(req.params.id)
    })()
    res.status(204).end()
  } catch (err) {
    if (err) {
      console.log(err)
      res.status(400).json({ err: err.message })
    }
  }
})

router.get('/:id/results', (req, res) => {
  const stmt = req.db.prepare(
    `SELECT votingnumber, signature FROM results WHERE id = ?;`
  )

  try {
    let rows = stmt.all(req.params.id)
    rows.map((r) => {
      r.signature = JSON.parse(r.signature)

      // Remove the vote message if production mode in enabled
      if (req.conf.mode === 'prod') r.signature.message = 'N/A'
    })
    return res.status(200).json(rows)
  } catch (err) {
    return res.status(403).json({ err: err.message })
  }
})

// Delete a result (only allowed in development mode)
router.delete('/:id/results/:votingnumber/delete', (req, res) => {
  if (conf.mode !== 'dev')
    return res.status(403).json({ err: 'ONLY_ALLOWED_IN_DEV_MODE' })

  console.log(
    `Deleting result for election (${req.params.id}) with votingnumber ${req.params.votingnumber}`
  )

  try {
    req.db.transaction(() => {
      req.db
        .prepare('DELETE FROM results WHERE id = ? AND votingnumber = ?;')
        .run([req.params.id, req.params.votingnumber])
      req.db
        .prepare(
          `UPDATE elections SET participants = participants - 1 WHERE id = ?`
        )
        .run(req.params.id)
    })()
    return res.status(204).end()
  } catch (err) {
    console.log('err: ', err)

    return res.status(403).json({ err: err.message })
  }
})

router.get('/mode', (req, res) => {
  return res.status(200).json({ mode: req.conf.mode })
})

module.exports = router
