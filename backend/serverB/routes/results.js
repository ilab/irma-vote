const express = require('express')

var router = express.Router()

/* This is publically available right now */
router.get('/:id', (req, res) => {
  const stmt = req.db.prepare(
    `SELECT votingnumber, signature FROM results WHERE id = ?;`
  )

  try {
    let rows = stmt.all(req.params.id)
    rows.map((r) => (r.signature = JSON.parse(r.signature)))
    return res.status(200).json(rows)
  } catch (err) {
    return res.status(403).json({ err: err.message })
  }
})

module.exports = router
