const sqlite3 = require('better-sqlite3')
const conf = require('./../config/conf.json')

let db = new sqlite3(conf.database_file)

db.transaction(() => {
  try {
    db.prepare(
      `CREATE TABLE IF NOT EXISTS elections (
        id TEXT PRIMARY KEY NOT NULL,
        title TEXT NOT NULL,
        description TEXT NOT NULL,
        question TEXT NOT NULL,
        options TEXT NOT NULL,
        end DATE NOT NULL,
        participants INTEGER NOT NULL,
        creation DATETIME DEFAULT (DATETIME('now'))
      );`
    ).run()
  } catch (err) {
    if (err && err.code != 'SQLITE_CONSTRAINT') {
      console.log(err.message)
      throw err
    }
  }

  // Insert a sample election
  try {
    db.prepare(
      `INSERT INTO elections (id, title, description, question, options, end, participants) VALUES (?, ?, ?, ?, ?, ?, ?);
    `
    ).run([
      'begroting_2021',
      'Buurtbegroting 2021',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      'Wat wordt de bestemming van de ruimte bij u in de buurt?',
      'Speeltuin,Parkeerplaats,Park',
      '2021-3-7',
      '0',
    ])
  } catch (err) {
    if (err && err.code != 'SQLITE_CONSTRAINT_PRIMARYKEY') {
      console.log(err.message)
      throw err
    }
  }

  // Create the result table
  // The voting number is a base-62 representation of a 256-bit integer
  // log_{62}(2^256) = 42.99... = 43 characters
  try {
    db.prepare(
      `CREATE TABLE IF NOT EXISTS results (
        id TEXT NOT NULL,
        votingnumber VARCHAR(43) NOT NULL,
        signature TEXT NOT NULL,
        PRIMARY KEY (id, votingnumber)
      );`
    ).run()
  } catch (err) {
    if (err) {
      throw err
    }
  }
})()

module.exports = db
