const sqlite3 = require('better-sqlite3')

const conf = require('./../config/conf.json')

// Connect to the database and initialize it
let db = new sqlite3(conf.database_file)

// Create a table and insert initial count
db.transaction(() => {
  // Create the elections table
  // Stores id, question, options, (start/end) date, nr of participants, creation date.
  try {
    db.prepare(
      `CREATE TABLE IF NOT EXISTS elections (
        id TEXT PRIMARY KEY NOT NULL,
        title TEXT NOT NULL,
        description TEXT NOT NULL,
        start DATE NOT NULL,
        end DATE NOT NULL,
        participants INTEGER NOT NULL,
        creation DATETIME DEFAULT (DATETIME('now'))
      );`
    ).run()
  } catch (err) {
    if (err && err.code != 'SQLITE_CONSTRAINT') {
      console.log(err.message)
      throw err
    }
  }

  // Insert a sample election
  try {
    db.prepare(
      `INSERT INTO elections (id, title, description, start, end, participants) VALUES (?, ?, ?, ?, ?, ?);
    `
    ).run([
      'begroting_2021',
      'Buurtbegroting 2021',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      '2021-2-28',
      '2021-3-7',
      '0',
    ])
  } catch (err) {
    if (err && err.code != 'SQLITE_CONSTRAINT_PRIMARYKEY') {
      console.log(err.message)
      throw err
    }
  }

  // Create the voting card table
  // id: election id
  // identity: voter identity
  try {
    db.prepare(
      `
    CREATE TABLE IF NOT EXISTS votingcards (
      id TEXT NOT NULL,
      identity TEXT NOT NULL,
      FOREIGN KEY(id) REFERENCES elections(id)
    )`
    ).run()
  } catch (err) {
    console.log(err.message)
    throw err
  }
})()

module.exports = db
