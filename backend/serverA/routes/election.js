const express = require('express')

var router = express.Router()

// example '/elections/radboudgebouw'
router.get('/:id', (req, res) => {
  var stmt = req.db.prepare('select * from elections WHERE id = ?')

  try {
    row = stmt.get(req.params.id)
    return res.status(200).json(row)
  } catch (err) {
    console.log(`Couldn't get election with name ${req.params.id}: ${err}`)
    return res.status(400).json({ error: err.message })
  }
})

module.exports = router
