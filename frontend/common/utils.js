const dateFormatOptions = {
  weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric',
}

const formatDate = (dateJson, locale) =>
  new Date(dateJson).toLocaleDateString(locale, dateFormatOptions)

module.exports = { formatDate }
