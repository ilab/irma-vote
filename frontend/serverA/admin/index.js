require('./../../node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')
require('./../../node_modules/bootstrap-table/dist/bootstrap-table.min.css')
require('./../../assets/style.scss')
require('bootstrap-datepicker')
require('bootstrap-table')
require('jquery-form')

const server = 'A'

function deleteElection(id) {
  fetch(`/api/v1/admin/${id}/delete`, { method: 'DELETE' }).then((res) => {
    if (res.status === 204) $('#overview').bootstrapTable('refresh')
  })
}

function displayMode() {
  fetch(`/api/v1/admin/mode`)
    .then((resp) => resp.json())
    .then((r) => {
      $('#mode').text(
        `Server ${server} (${
          r.mode == 'dev' ? 'Development' : 'Production'
        } version)`
      )
    })
    .catch((err) => {
      console.log("couldn't get mode:", err.message)
    })
}

function initTable() {
  $('#overview').bootstrapTable({
    showRefresh: true,
    columns: [
      { field: 'id', title: 'Election' },
      { field: 'title', title: 'Title' },
      { field: 'start', title: 'Start' },
      { field: 'end', title: 'End' },
      { field: 'creation', title: 'Creation date' },
      { field: 'participants', title: 'Participants' },
      {
        field: 'actions',
        title: 'Actions',
        formatter: () => {
          return `
          <button type='button' style='font-size:17px' class='list btn btn-outline-primary border-0' ><i class="far fa-list-alt"></i></button>
          <button type='button' style='font-size:17px' class='remove btn btn-outline-danger border-0' ><i class='far fa-trash-alt'></i></button>
          <button type='button' style='font-size:17px' class='vote btn btn-outline-success border-0' ><i class="fas fa-vote-yea"></i></button>
          `
        },
        events: {
          'click .list': (e, value, row, index) => {
            window.open(`/admin/retrieved?id=${row.id}`, '_blank')
          },
          'click .remove': (e, value, row, index) => deleteElection(row.id),
          'click .vote': (e, value, row, index) => {
            window.open(`/user/?id=${row.id}`, '_blank')
          },
        },
      },
    ],
    onLoadSuccess: () => {
      console.log('table loaded')
    },
    onLoadError: () => {
      console.log('failed to load table')
      $('#alert_placeholder').html(
        `<div class="alert alert-warning" role="alert">U bent niet ingelogd. Log <a href="/admin/login">hier</a> in.</div>`
      )
    },
  })
}

function initForm() {
  $('#election-start')
    .datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
    })
    .on('changeDate', function (selected) {
      var minDate = new Date(selected.date.valueOf())
      $('#election-start').datepicker('setStartDate', minDate)
    })

  $('#election-end')
    .datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
    })
    .on('changeDate', function (selected) {
      var minDate = new Date(selected.date.valueOf())
      $('#election-end').datepicker('setEndDate', minDate)
    })

  $('#new').ajaxForm({
    url: '/api/v1/admin/new',
    dataType: 'json',
    type: 'POST',
    success: () => {
      $('#alert_placeholder').html(
        `<div class="alert alert-success" role="alert">Nieuwe verkiezing aangemaakt</div>`
      )
      $('#overview').bootstrapTable('refresh')
    },
    error: (res) => {
      $('#alert_placeholder').html(
        `<div class="alert alert-warning" role="alert">Verkiezing aanmaken mislukt: ${res.responseJSON.err}</div>`
      )
    },
  })
}

$(document).ready(function () {
  $('#basic-addon1').text(
    window.location.protocol + '//' + window.location.hostname + '/user/?id='
  )
  displayMode()
  initTable()
  initForm()
})
