require('./../../assets/style.scss')
require('./../../node_modules/bootstrap-table/dist/bootstrap-table.min.css')
require('bootstrap-table')
require('jquery-form')

const server = 'B'

let state = {
  id: undefined,
  mode: undefined,
}

function getMode() {
  return fetch(`/api/v1/admin/mode`)
    .then((resp) => resp.json())
    .then((r) => {
      state.mode = r.mode === 'dev' ? 'dev' : 'prod'
    })
    .catch((err) => {
      console.log("couldn't get mode:", err.message)
    })
}

function deleteResult(votingnumber) {
  fetch(`/api/v1/admin/${state.id}/results/${votingnumber}/delete`, {
    method: 'DELETE',
  }).then((resp) => {
    console.log('succes')
    if (resp.status === 204) $('#votingcards-table').bootstrapTable('refresh')
  })
}

function initTable() {
  var table = $('#votingcards-table')
  table.attr('data-url', `/api/v1/admin/${state.id}/results`)
  table.bootstrapTable({
    showRefresh: true,
    columns: [
      // TODO: Include a count?
      { field: 'votingnumber', title: 'Stemnummer' },
      {
        field: 'signature.message',
        title: 'Stem',
      },
      { field: 'signature.timestamp.Time', title: 'Tijd' },
      ...(state.mode === 'dev'
        ? [
            {
              field: 'actions',
              title: 'Actions',
              formatter: () => {
                return `<button type='button' style='font-size:17px' class='remove btn btn-outline-danger border-0' ><i class='far fa-trash-alt'></i></button>`
              },
              events: {
                'click .remove': (e, value, row, index) =>
                  deleteResult(row.votingnumber),
              },
            },
          ]
        : []),
    ],
    onLoadSuccess: (data) =>
      console.log('table succesfully loaded, data: ', data),
    onLoadError: (err) => console.log('failed to load table: ', err),
  })
}

$(document).ready(async () => {
  const queryString = window.location.search
  const urlParams = new URLSearchParams(queryString)
  state.id = urlParams.get('id')
  if (!state.id) throw new Error('NO ID')

  await getMode()

  $('#mode').text(
    `Server ${server} (${
      state.mode == 'dev' ? 'Development' : 'Production'
    } version)`
  )

  initTable()
})
