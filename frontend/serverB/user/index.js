const irma = require('@privacybydesign/irma-frontend')
const style = require('./../../assets/style.scss')
import { formatDate } from './../../common/utils'

const SpeelTuinImg = require('./../assets/images/playground.800.jpg')
const ParkeerplaatsImg = require('./../assets/images/parking.800.jpg')
const ParkImg = require('./../assets/images/parc.800.jpg')

const locale = ['nl-NL']

let id = undefined

const mapOptionToImg = {
  Speeltuin: SpeelTuinImg,
  Parkeerplaats: ParkeerplaatsImg,
  Park: ParkImg,
}

function load() {
  const queryString = window.location.search
  const urlParams = new URLSearchParams(queryString)
  id = urlParams.get('id')
  if (!id) throw new Error('NO_ID')

  return fetch(`/api/v1/election/${id}`)
    .then((resp) => resp.json())
    .then((json) => {
      $('#election-date-end').text(formatDate(json.end, locale))
      $('#election-question').text(json.question)
      json.options.split(',').forEach((option, i) => {
        $('#options').append(
          `
           <div class="form-check">
             <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault${i}" />
             <label class="form-check-label" for="flexRadioDefault${i}">
               ${option}<br />
               ${
                 mapOptionToImg[option]
                   ? `
                      <img
                        src=${mapOptionToImg[option]}
                        class="rounded w-100"
                        style="object-fit: cover; max-width: 200px; max-height: 150px"
                      />
                      `
                   : ''
               }
             </label>
           </div>
          `
        )
      })
    })
}

function registerChoiceListener() {
  document
    .getElementById('confirm')
    .addEventListener('click', function (event) {
      event.target.disabled = true
      event.target.classList.add('d-none')
      let inputs = document.getElementsByTagName('input')
      console.log(inputs)
      let text = ''
      for (let i = 0; i < inputs.length; ++i) {
        let input = inputs[i]
        input.disabled = true
        if (input.checked) {
          text = input.labels[0].innerHTML
        }
      }
      let after = document.getElementById('after-confirm')
      after.classList.remove('d-none')
      after.classList.add('d-block')
      console.log(event)
      // strip HTML
      text = text.replace(/(<([^>]+)>)/gi, '')
      text = text.trim()
      console.log(text)

      let options = {
        debugging: true,
        element: '#irma-web-form',
        session: {
          url: `/api/v1/vote/${id}`,
          start: {
            url: (o) => `${o.url}/start`,
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ text: text }),
          },
          mapping: {
            sessionPtr: (r) => r,
          },
          result: {
            url: (o) => `${o.url}/finish`,
            parseResponse: (r) => r.status,
          },
        },
      }

      const irmaWeb = irma.newWeb(options)
      irmaWeb
        .start()
        .then((result) => {
          // wait two seconds to display check mark
          return new Promise((resolve) =>
            setTimeout(() => resolve(result), 2000)
          )
        })
        .then((result) => {
          if (result !== 200) throw new Error('signature failed')
          console.log('signature completed')
          //window.location.href = "next.html";
        })
        .catch((error) => console.error('error: ', error))
    })
}

$(document).ready(() => {
  load()
    .then(registerChoiceListener())
    .catch((err) => {
      console.log('err: ', err)
    })
})
